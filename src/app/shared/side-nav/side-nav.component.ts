import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "../service/api.service";

@Component({
  selector: "app-side-nav",
  templateUrl: "./side-nav.component.html",
  styleUrls: ["./side-nav.component.css"]
})
export class SideNavComponent implements OnInit {
  newestApps: any = [];
  textSearch: string = null;
  searchedApps: any = [];
  token: any;
  currentRoute: string;
  filters: any = [];
  appsList: any = [];

  constructor(private router: Router, private apiService: ApiService) {}

  ngOnInit() {
    this.getfilters();
    this.textSearch = "";
  }

  navigateAppList(selectedItem) {
    this.textSearch = "";
    this.apiService.getSelectedItemApps(selectedItem).subscribe((data: any) => {
      this.appsList = data.list;
      this.apiService.$selectedOptionSubject.next({
        list: this.appsList,
        selectedName: selectedItem
      });
      this.router.navigate(["/app-list"]);
    });

    // switch (selectedItem) {
    //   case "All Apps":
    //     this.apiService.getAllApps().subscribe((data: any) => {
    //       this.appsList = data.list;
    //       this.apiService.$selectedOptionSubject.next({
    //         list: this.appsList,
    //         selectedName: selectedItem
    //       });
    //       this.router.navigate(["/app-list"]);
    //     });
    //     break;
    //   case "Featured":
    //     this.apiService.getFeaturedApps().subscribe((data: any) => {
    //       this.appsList = data.list;
    //       this.apiService.$selectedOptionSubject.next({
    //         list: this.appsList,
    //         selectedName: selectedItem
    //       });
    //       this.router.navigate(["/app-list"]);
    //     });
    //     break;
    //   case "Popular":
    //     this.apiService.getPopularApps().subscribe((data: any) => {
    //       this.appsList = data.list;
    //       this.apiService.$selectedOptionSubject.next({
    //         list: this.appsList,
    //         selectedName: selectedItem
    //       });
    //       this.router.navigate(["/app-list"]);
    //     });
    //     break;
    //   case "Newest":
    //     this.apiService.getNewestApps().subscribe((data: any) => {
    //       this.appsList = data.list;
    //       this.apiService.$selectedOptionSubject.next({
    //         list: this.appsList,
    //         selectedName: selectedItem
    //       });
    //       this.router.navigate(["/app-list"]);
    //     });
    //     break;
    //   case "Analytics":
    //     this.apiService.getAnalyticsApps().subscribe((data: any) => {
    //       this.appsList = data.list;
    //       this.apiService.$selectedOptionSubject.next({
    //         list: this.appsList,
    //         selectedName: selectedItem
    //       });
    //       this.router.navigate(["/app-list"]);
    //     });
    //     break;
    //   case "Communication":
    //     this.apiService.getCommunicationApps().subscribe((data: any) => {
    //       this.appsList = data.list;
    //       this.apiService.$selectedOptionSubject.next({
    //         list: this.appsList,
    //         selectedName: selectedItem
    //       });
    //       this.router.navigate(["/app-list"]);
    //     });
    //     break;
    //   case "Customer Support":
    //     this.apiService.getCustomerSupportApps().subscribe((data: any) => {
    //       this.appsList = data.list;
    //       this.apiService.$selectedOptionSubject.next({
    //         list: this.appsList,
    //         selectedName: selectedItem
    //       });
    //       this.router.navigate(["/app-list"]);
    //     });
    //     break;
    //   case "File Management":
    //     this.apiService.getFileManagementApps().subscribe((data: any) => {
    //       this.appsList = data.list;
    //       this.apiService.$selectedOptionSubject.next({
    //         list: this.appsList,
    //         selectedName: selectedItem
    //       });
    //       this.router.navigate(["/app-list"]);
    //     });
    //     break;
    //   case "Productivity":
    //     this.apiService.getProductivityApps().subscribe((data: any) => {
    //       this.appsList = data.list;
    //       this.apiService.$selectedOptionSubject.next({
    //         list: this.appsList,
    //         selectedName: selectedItem
    //       });
    //       this.router.navigate(["/app-list"]);
    //     });
    //     break;
    //   case "Developer Tools":
    //     this.apiService.getDeveloperToolsApps().subscribe((data: any) => {
    //       this.appsList = data.list;
    //       this.apiService.$selectedOptionSubject.next({
    //         list: this.appsList,
    //         selectedName: selectedItem
    //       });
    //       this.router.navigate(["/app-list"]);
    //     });
    //     break;
    //   default: {
    //     break;
    //   }
    // }
  }

  seachApp(searchTextaValue: string) {
    if (this.router.url.includes("/app-list")) {
      this.apiService.$searchQuerySubject.next(searchTextaValue);
    } else {
      this.apiService.$searchQuerySubject.next(searchTextaValue);
      this.router.navigate(["/app-list"]);
    }
  }

  getfilters() {
    this.apiService.getCollectionCategoryfilters().subscribe((data: any) => {
      this.filters = data.list;
    });
  }
}
