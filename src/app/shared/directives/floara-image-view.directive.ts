import {Directive, ElementRef, Input, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[froalaImageView]'
})
export class FroalaImageViewDirective implements OnInit {
  @Input() froalaImageView: Object;

  constructor(private _elementRef: ElementRef, private _renderer2: Renderer2) { }

  ngOnInit() {
    this.addFroalaViewAttributes();
  }

  private addFroalaViewAttributes() {
    Object.entries(this.froalaImageView).forEach(([key, value]) => {
      this._renderer2.setAttribute(this._elementRef.nativeElement, key, value);
    });
  }
}
